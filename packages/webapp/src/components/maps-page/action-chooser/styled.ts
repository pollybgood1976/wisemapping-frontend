import MenuItem from '@mui/material/MenuItem';
import withStyles from '@mui/styles/withStyles';

export const StyledMenuItem = withStyles({
    root: {
        width: '300px',
    },
})(MenuItem);
