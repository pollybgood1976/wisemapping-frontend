import fadeEffect from './FadeEffect';
import shape from './Shape';

export default {
  FadeEffect: fadeEffect,
  Shape: shape,
};
