1 SaberMás
	1.1 Utilización de medios de expresión artística, digitales y analógicos
	1.2 Precio también limitado: 100-120?
	1.3 Talleres temáticos
		1.3.1 Naturaleza
			1.3.1.1 Animales, Plantas, Piedras
		1.3.2 Arqueología
		1.3.3 Energía
		1.3.4 Astronomía
		1.3.5 Arquitectura
		1.3.6 Cocina
		1.3.7 Poesía
		1.3.8 Culturas Antiguas
			1.3.8.1 Egipto, Grecia, China...
		1.3.9 Paleontología
	1.4 Duración limitada: 5-6 semanas
	1.5 Niños y niñas que quieren saber más
	1.6 Alternativa a otras actividades de ocio
	1.7 Uso de la tecnología durante todo el proceso de aprendizaje
	1.8 Estructura PBL: aprendemos cuando buscamos respuestas a nuestras propias preguntas 
	1.9 Trabajo basado en la experimentación y en la investigación
	1.10 De 8 a 12 años, sin separación por edades
	1.11 Máximo 10/1 por taller
	1.12 Actividades centradas en el contexto cercano
	1.13 Flexibilidad en el uso de las lenguas de trabajo (inglés, castellano, esukara?)
	1.14 Complementamos el trabajo de la escuela
		1.14.1 Cada uno va a su ritmo, y cada cual pone sus límites
		1.14.2 Aprendemos todos de todos
		1.14.3 Valoramos lo que hemos aprendido
		1.14.4 SaberMás trabaja con, desde y para la motivación
		1.14.5 Trabajamos en equipo en nuestros proyectos 
